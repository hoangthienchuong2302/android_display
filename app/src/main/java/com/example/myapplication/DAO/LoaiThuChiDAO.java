package com.example.myapplication.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.myapplication.database.DbHelper;
import com.example.myapplication.model.LoaiThuChi;

import java.util.ArrayList;
import java.util.List;

public class LoaiThuChiDAO {
    SQLiteDatabase db;
    DbHelper dbHelper;

    public LoaiThuChiDAO(Context context) {
        dbHelper = new DbHelper( context );

    }

    public void insert(LoaiThuChi item) {
        db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put( "tenLoai", item.getTenLoai() );
        values.put( "trangThai", item.getTrangThai() );

        db.insert( "LOAI_TC", null, values );
    }

    public void update(LoaiThuChi item) {
        db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put( "maLoai", item.getMaLoai() );
        values.put( "tenLoai", item.getTenLoai() );
        values.put( "trangThai", item.getTrangThai() );
        db.update( "LOAI_TC", values, "maLoai=?", new String[]{item.getMaLoai()+""} );
    }

    public void delete(String maLoai) {
        db = dbHelper.getWritableDatabase();
        db.delete( "LOAI_TC", "maLoai=?", new String[]{maLoai} );
    }

    //get nhieu tham so
    public ArrayList<LoaiThuChi> get() {
        ArrayList<LoaiThuChi> list = new ArrayList<LoaiThuChi>();
        db = dbHelper.getWritableDatabase();
        Cursor c = db.query("LOAI_TC", null,null,null,null,null,null);
        if (c.moveToFirst()) {
            while (c.moveToNext()) {
                int maloai = Integer.parseInt( c.getString( 0 ) );
                String tenloai = c.getString( 1 );
                String trangthai = c.getString( 2 );
                LoaiThuChi item = new LoaiThuChi( maloai, tenloai, trangthai );
                list.add( item );
            }
        }
        return list;
    }
}

