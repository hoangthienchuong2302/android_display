package com.example.myapplication.DAO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.myapplication.database.DbHelper;
import com.example.myapplication.model.KhoanThuChi;

import java.util.ArrayList;
import java.util.Date;

public class ThongKeDAO {
    private SQLiteDatabase db;
    DbHelper dbHelper;
    public ThongKeDAO(Context context) {
        dbHelper = new DbHelper( context );

    }
    public double getTotalthu(String trangThai){
        double total = 0.0;
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT SUM(tien) FROM KHOAN_TC INNER JOIN LOAI_TC ON KHOAN_TC.maLoai = LOAI_TC.maLoai AND LOAI_TC.trangThai LIKE '"+trangThai+"'",null);
        if (cursor.moveToFirst()){
            do {
                total = cursor.getDouble(0);

            }while (cursor.moveToNext());
        }
        return total;
    }
    public ArrayList<KhoanThuChi> getKhoanThu_Chi(String trangthai){
        ArrayList<KhoanThuChi> ds_giaodich = new ArrayList<>();
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM KHOAN_TC INNER JOIN LOAI_TC ON KHOAN_TC.maLoai = LOAI_TC.maLoai AND LOAI_TC.trangThai LIKE '"+trangthai+"'",null);
        if (cursor.moveToFirst()){
            do {
                Date date1 = null;
                int Magiaodich = cursor.getInt(0);
                String Tieude = cursor.getString(1);
                String Ngay = cursor.getString(2);
                double Tien = Double.parseDouble(cursor.getString(3));
                String Mota = cursor.getString(4);
                int Maloai = cursor.getInt(5);
                ds_giaodich.add(new KhoanThuChi(Magiaodich,Tieude,Ngay,Tien,Mota,Maloai));
            } while (cursor.moveToNext());
        }
        return ds_giaodich;
    }
    public ArrayList<KhoanThuChi> get_Tk_Thu(String date1, String date2,String trangThai){
        ArrayList<KhoanThuChi> ds_giaodich = new ArrayList<>();
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM KHOAN_TC,LOAI_TC WHERE ngay BETWEEN '"+date1+"' AND '"+date2+"' AND KHOAN_TC.maLoai = LOAI_TC.maLoai AND LOAI_TC.trangthai LIKE '"+trangThai+"'" ,null);
        if (cursor.moveToFirst()){
            do {
                int Magiaodich = cursor.getInt(0);
                String Tieude = cursor.getString(1);
                String Ngay = cursor.getString(2);
                double Tien = Double.parseDouble(cursor.getString(3));
                String Mota = cursor.getString(4);
                int Maloai = cursor.getInt(5);
                ds_giaodich.add(new KhoanThuChi(Magiaodich,Tieude,Ngay,Tien,Mota,Maloai));
            } while (cursor.moveToNext());
        }
        return ds_giaodich;
    }
    public double getTotalthubyDate(String date1, String date2,String trangThai){
        double total = 0.0;
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT SUM(tien) FROM KHOAN_TC,LOAI_TC WHERE ngay BETWEEN '"+date1+"' AND '"+date2+"' AND KHOAN_TC.maLoai = LOAI_TC.maLoai AND LOAI_TC.trangThai LIKE '"+trangThai+"'",null);
        if (cursor.moveToFirst()){
            do {
                total = cursor.getDouble(0);

            }while (cursor.moveToNext());
        }
        return total;
    }

}
