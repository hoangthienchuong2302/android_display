package com.example.myapplication.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.myapplication.database.DbHelper;
import com.example.myapplication.model.KhoanThuChi;
import com.example.myapplication.model.LoaiThuChi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class KhoanThuChiDAO {
    private SQLiteDatabase db;
    DbHelper dbHelper;
    public KhoanThuChiDAO(Context context) {
        dbHelper = new DbHelper( context );

    }

    public long insert(KhoanThuChi item){
        db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("tieuDe", item.getTieuDe());
        values.put("ngay", item.getNgay());
        values.put("tien", item.getTien());
        values.put("moTa", item.getMoTa());
        values.put("maLoai", item.getMaLoai());
        return db.insert("KHOAN_TC", null, values);
    }

    public int update(KhoanThuChi item) {
        db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("tieuDe", item.getTieuDe());
        values.put("ngay", item.getNgay());
        values.put("tien", item.getTien());
        values.put("moTa", item.getMoTa());
        return db.update("KHOAN_TC", values, "maGD=?", new String[]{String.valueOf(item.getMaGD())});
    }

    public int delete(String maLoai) {
        db = dbHelper.getWritableDatabase();
        return db.delete("KHOAN_TC", "maGD=?", new String[]{maLoai});
    }

    public ArrayList<KhoanThuChi> getKhoanThuChi(String trangThai){
        ArrayList<KhoanThuChi> ds_giaodich = new ArrayList<>();
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM KHOAN_TC INNER JOIN LOAI_TC ON KHOAN_TC.maLoai = LOAI_TC.maLoai " +
                "AND LOAI_TC.trangThai LIKE '"+ trangThai +"'",null);
        if (cursor.moveToFirst()){
            do {
                int Magiaodich = cursor.getInt(0);
                String Tieude = cursor.getString(1);
                String Ngay = cursor.getString(2);
                double Tien = Double.parseDouble(cursor.getString(3));
                String Mota = cursor.getString(4);
                int Maloai = cursor.getInt(5);
                ds_giaodich.add(new KhoanThuChi(Magiaodich,Tieude,Ngay,Tien,Mota,Maloai));
            } while (cursor.moveToNext());

        }
        return ds_giaodich;
    }

    public ArrayList<LoaiThuChi> getAll(String trangthai) {
        db = dbHelper.getWritableDatabase();
        ArrayList<LoaiThuChi> list = new ArrayList<LoaiThuChi>();

        Cursor c = db.rawQuery( "SELECT * FROM LOAI_TC WHERE trangThai= '"+ trangthai +"'",null );
        if (c.moveToFirst()) {
            while (c.moveToNext()) {
                int maloai = Integer.parseInt( c.getString( 0 ) );
                String tenloai = c.getString( 1 );
                String tt = c.getString( 2 );
                LoaiThuChi item = new LoaiThuChi( maloai, tenloai, tt );
                list.add( item );
            }
        }
        return list;
    }

    public String getTrangthai(int idLoai,int idKhoan){
        String ten = "";
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT trangThai FROM LOAI_TC,KHOAN_TC WHERE LOAI_TC.maLoai = '"+idLoai+"' AND KHOAN_TC.maGD = '"+idKhoan+"'",null);
        if (cursor.moveToFirst()){
            do {
                ten = cursor.getString(0);
            }while (cursor.moveToNext());
        }
        return ten;
    }
    public String getTen(String id){
        String ten = "";
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT tenloai FROM LOAI_TC WHERE maloai = '"+id+"'",null);
        if (cursor.moveToFirst()){
            do {
                ten = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        return ten;
    }

}
