package com.example.myapplication.Adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.DAO.KhoanThuChiDAO;
import com.example.myapplication.DAO.LoaiThuChiDAO;
import com.example.myapplication.R;
import com.example.myapplication.dialog.BottomSheet_edit_KhoanTC;
import com.example.myapplication.fragment.Fragment_phanLoaiTC;
import com.example.myapplication.model.KhoanThuChi;

import java.util.ArrayList;


public class KhoanThuAdapter extends RecyclerView.Adapter< KhoanThuAdapter.KhoanTCHolder> {
    public ArrayList<KhoanThuChi> ds_khoanTC;
    private Context context;
    public KhoanThuChi khoanThuChi;
    private KhoanThuChiDAO khoanThuChiDAO;

    private Fragment_phanLoaiTC fragment_phanLoaiTC;


    public KhoanThuAdapter(ArrayList<KhoanThuChi> ds_khoanTC, Context context) {
        this.ds_khoanTC = ds_khoanTC;
        this.context = context;
    }

    public static class KhoanTCHolder extends RecyclerView.ViewHolder {
        public View view;
        public TextView tvTieuDe;
        public TextView tvTien;
        public TextView tvNgay;
        public ImageView imgDelete;
        public ImageView imgEdit;

        public KhoanTCHolder(View itemView) {
            super( itemView );
            tvTieuDe = itemView.findViewById( R.id.tvTieuDe );
            tvTien = itemView.findViewById( R.id.tvTien );
            tvNgay = itemView.findViewById( R.id.tvNgay );
            imgDelete = itemView.findViewById( R.id.imgDelete );
            imgEdit = itemView.findViewById( R.id.imgEdit );
        }
    }


    @Override
    public KhoanThuAdapter.KhoanTCHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from( parent.getContext() ).inflate( R.layout.item_khoan_thu, parent, false );

        KhoanTCHolder khoanTCHolder= new KhoanTCHolder( v );

        return khoanTCHolder;
    }


    @Override
    public void onBindViewHolder(final KhoanTCHolder holder, final int position) {

        final KhoanThuChi khoanThuChi = ds_khoanTC.get(position);
        holder.tvTieuDe.setText( ds_khoanTC.get( position ).getTieuDe() );
        holder.tvTien.setText( ds_khoanTC.get( position ).getTien()+"" );
        holder.tvNgay.setText( ds_khoanTC.get( position ).getNgay()+"" );
        khoanThuChiDAO = new KhoanThuChiDAO(context);

//        trangThai= khoanThuChiDAO.getTrangthai( ds_khoanTC.get( position ).getMaLoai(), ds_khoanTC.get( position ).getMaGD() );
        holder.imgDelete.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Delete");
                builder.setMessage("Ban co muon xoa "+ds_khoanTC.get( position ).getTieuDe()+ "khong?");
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //Goi function Delete
                                khoanThuChiDAO = new KhoanThuChiDAO(context);
                                khoanThuChiDAO.delete( ds_khoanTC.get(position).getMaGD()+"");
                                ds_khoanTC.remove( position );
                                //ds_khoanTC.clear();
                                Toast.makeText(context, "Xóa thành công ",Toast.LENGTH_SHORT ).show();
                                notifyDataSetChanged();

                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

                Toast.makeText( context, "vi tri"+ds_khoanTC.get( position ).getMaGD(), Toast.LENGTH_SHORT ).show();
            }
        } );

        holder.imgEdit.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString("maGD", ds_khoanTC.get(position).getMaGD()+"");
                args.putString("tieude", ds_khoanTC.get(position).getTieuDe()+"");
                args.putString("ngay", ds_khoanTC.get(position).getNgay()+"");
                args.putString("moTa", ds_khoanTC.get(position).getMoTa()+"");
                args.putDouble("tien", ds_khoanTC.get(position).getTien());
                args.putString("maloai", ds_khoanTC.get(position).getMaLoai()+"");
                khoanThuChiDAO= new KhoanThuChiDAO( context );
                String tt =khoanThuChiDAO.getTrangthai( ds_khoanTC.get( position ).getMaLoai(),ds_khoanTC.get( position ).getMaGD() );
                args.putString( "trangThai",tt );

                BottomSheet_edit_KhoanTC bottomSheet_edit_khoanTC= new BottomSheet_edit_KhoanTC();
                bottomSheet_edit_khoanTC.setArguments( args );
                bottomSheet_edit_khoanTC.show(((AppCompatActivity) context).getSupportFragmentManager(),bottomSheet_edit_khoanTC.getTag());;
            }
        });

    }

    @Override
    public int getItemCount() {
        return ds_khoanTC.size();
    }



}
