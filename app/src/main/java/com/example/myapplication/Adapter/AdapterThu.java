package com.example.myapplication.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.model.LoaiThuChi;

import java.util.ArrayList;

public class AdapterThu extends BaseAdapter {
    Context context;
    ArrayList<LoaiThuChi> ds_thu;
    TextView tv_sp_Thu;

    public AdapterThu(Context context, ArrayList<LoaiThuChi> ds_thu) {
        this.context = context;
        this.ds_thu = ds_thu;
    }

    @Override
    public int getCount() {
        return ds_thu.size();
    }

    @Override
    public Object getItem(int i) {
        return ds_thu.get( i );
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = ((Activity)context).getLayoutInflater().inflate( R.layout.item_sp_thu,viewGroup,false );
        tv_sp_Thu= view.findViewById( R.id.tv_sp_Thu );
        tv_sp_Thu.setText( ds_thu.get( i ).getTenLoai() );
        return view;
    }
}
