package com.example.myapplication.Adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.DAO.LoaiThuChiDAO;
import com.example.myapplication.R;
import com.example.myapplication.fragment.Fragment_phanLoaiTC;
import com.example.myapplication.fragment.Fragment_thongKe;
import com.example.myapplication.model.LoaiThuChi;

import java.util.ArrayList;

import static com.example.myapplication.fragment.Fragment_phanLoaiTC.phanLoaiTC_adapter;
import static com.example.myapplication.fragment.Fragment_phanLoaiTC.recyclerView_PhanLoai;


public class PhanLoaiTC_Adapter extends RecyclerView.Adapter< PhanLoaiTC_Adapter.PhanLoaiTcHolder> {
    private ArrayList<LoaiThuChi> ds_phanLoai;
    private Context context;
    private LoaiThuChiDAO loaiThuChiDAO;
    Dialog dialog;
    EditText editTenLoai, editMaLoai;
    Spinner spPhanLoai;
    Button btnCancel, btnSave;


    public PhanLoaiTC_Adapter(ArrayList<LoaiThuChi> ds_phanLoai, Context context) {
        this.ds_phanLoai = ds_phanLoai;
        this.context = context;
    }

    public static class PhanLoaiTcHolder extends RecyclerView.ViewHolder {
        public View view;
        public TextView tvTen;
        public TextView tvTrangThai;
        public ImageView imgDelete;
        public ImageView imgEdit;

        public PhanLoaiTcHolder(View itemView) {
            super( itemView );
            tvTen = itemView.findViewById( R.id.tvTen );
            tvTrangThai = itemView.findViewById( R.id.tvTrangThai );
            imgDelete = itemView.findViewById( R.id.imgDelete );
            imgEdit = itemView.findViewById( R.id.imgEdit );
        }
    }


    @Override
    public PhanLoaiTC_Adapter.PhanLoaiTcHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from( parent.getContext() ).inflate( R.layout.item_phanloaitc, parent, false );

        PhanLoaiTcHolder phanLoaiTcHolder = new PhanLoaiTcHolder( v );

        return phanLoaiTcHolder;
    }

    @Override
    public void onBindViewHolder(final PhanLoaiTcHolder holder, final int position) {
         final LoaiThuChi loaiThuChi = ds_phanLoai.get(position);
        holder.tvTen.setText( ds_phanLoai.get( position ).getTenLoai() );
        holder.tvTrangThai.setText( ds_phanLoai.get( position ).getTrangThai() );
        holder.imgDelete.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Delete");
                builder.setMessage("Ban co muon xoa "+ds_phanLoai.get( position ).getTenLoai()+ "khong?");
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //Goi function Delete
                                loaiThuChiDAO= new LoaiThuChiDAO(context);
                                loaiThuChiDAO.delete( ds_phanLoai.get(position).getMaLoai()+"");
                                Toast.makeText(context, "Xóa thành công "+ds_phanLoai.get( position ).getTenLoai(),Toast.LENGTH_SHORT ).show();
                                capNhat();

                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        } );

        holder.imgEdit.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom_dialog_phanloai);

                editTenLoai= dialog.findViewById(R.id.edtTenLoai);
                editMaLoai=dialog.findViewById( R.id.edtMaLoai );
                btnCancel = dialog.findViewById(R.id.btnCacel);
                btnSave = dialog.findViewById(R.id.btnSave);
                spPhanLoai=dialog.findViewById(R.id.spPhanLoai);

                ds_phanLoai = new ArrayList<LoaiThuChi>();
                loaiThuChiDAO= new LoaiThuChiDAO(context);
                ds_phanLoai = (ArrayList<LoaiThuChi>)loaiThuChiDAO.get();

                ArrayAdapter<CharSequence> adapter= ArrayAdapter.createFromResource(context,
                        R.array.phanLoai,android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

                spPhanLoai.setAdapter(adapter);
                editMaLoai.setText(String.valueOf( loaiThuChi.getMaLoai() ));
                editMaLoai.setEnabled(false);
                editTenLoai.setText(loaiThuChi.getTenLoai());

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loaiThuChiDAO = new LoaiThuChiDAO( context );

                        loaiThuChi.setMaLoai(Integer.parseInt( editMaLoai.getText().toString() )) ;
                        loaiThuChi.setTenLoai(editTenLoai.getText().toString());
                        loaiThuChi.setTrangThai(spPhanLoai.getSelectedItem().toString());
                        String tenloai = editTenLoai.getText().toString();
                        String trangthai = spPhanLoai.getSelectedItem().toString();
                        loaiThuChiDAO.update(new LoaiThuChi(ds_phanLoai.get( position ).getMaLoai(), tenloai,trangthai  ));
                        capNhat();
                        Toast.makeText(context, "Save", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

    }


    @Override
    public int getItemCount() {

        return ds_phanLoai.size();
    }


    @Override
    public long getItemId(int position) {
        return super.getItemId( position );
    }

    public void capNhat(){
        loaiThuChiDAO= new LoaiThuChiDAO( context);
        ds_phanLoai= loaiThuChiDAO.get();
        phanLoaiTC_adapter= new PhanLoaiTC_Adapter( ds_phanLoai,context );
        recyclerView_PhanLoai.setAdapter( phanLoaiTC_adapter );
    }
}
