package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.myapplication.fragment.Fragment_khoanTC;
import com.example.myapplication.fragment.Fragment_phanLoaiTC;
import com.example.myapplication.fragment.Fragment_thongKe;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity {
    DrawerLayout dr;
    NavigationView nav;
    Toolbar tb;
    ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN );
        getWindow().setStatusBarColor( ContextCompat.getColor( MainActivity.this,R.color.colorAccent ) );

        dr= findViewById(R.id.dr);
        tb= findViewById(R.id.tb);
        nav=findViewById(R.id.nav);
        dr.addDrawerListener(drawerToggle);
        drawerToggle=setupDrawer();
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();



        setSupportActionBar(tb);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(savedInstanceState==null){
            nav.setCheckedItem(R.id.thongKe);
            getSupportFragmentManager().beginTransaction().replace(R.id.fr,new Fragment_thongKe()).commit();
        }
        nav.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Fragment fragment = null;
                Class frClass = null;

                switch (menuItem.getItemId()){
                    case R.id.khoanThuChi:
                        frClass= Fragment_khoanTC.class;
                        break;
                    case R.id.phanLoaiThuChi:
                        frClass= Fragment_phanLoaiTC.class;
                        break;
                    case R.id.thongKe:
                        frClass= Fragment_thongKe.class;
                        break;
                    case R.id.gioiThieu:

                        frClass= Fragment_thongKe.class;
                        Toast.makeText(MainActivity.this,"Đây là: Giới thiệu",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.thoat:
                        frClass= Fragment_thongKe.class;
                        Toast.makeText(MainActivity.this,"Đây là: Thoát",Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        frClass=Fragment_phanLoaiTC.class;
                }
                try{
                    fragment =(Fragment)frClass.newInstance();
                }catch (Exception e){
                    e.printStackTrace();
                }

                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.fr, fragment).commit();

                menuItem.setChecked(true);
                setTitle(menuItem.getTitle());
                dr.closeDrawers();
                return false;
            }
        });
    }

    private ActionBarDrawerToggle setupDrawer(){
        return new ActionBarDrawerToggle(MainActivity.this,dr,tb,R.string.Open,R.string.Close);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(drawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void onBackPressed(){
        if(dr.isDrawerOpen( GravityCompat.START )){
            dr.closeDrawer( GravityCompat.START );
        }else{
            super.onBackPressed();
        }
    }
}
