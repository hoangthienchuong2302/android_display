package com.example.myapplication.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import androidx.viewpager.widget.ViewPager;

import com.example.myapplication.Adapter.TableAdapter;
import com.example.myapplication.R;

import com.example.myapplication.tableFragment.Tab_KhoanChi;
import com.example.myapplication.tableFragment.Tab_KhoanThu;
import com.google.android.material.tabs.TabLayout;


public class Fragment_khoanTC extends Fragment {
    private TableAdapter tableAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_khoantc,container,false);

        tabLayout=  view.findViewById( R.id.tabLayout );
        viewPager= view.findViewById( R.id.viewPager );

        tableAdapter= new TableAdapter( getActivity().getSupportFragmentManager() );
        tableAdapter.addFragment( new Tab_KhoanThu(),"Khoản thu" );
        tableAdapter.addFragment( new Tab_KhoanChi(),"KHoản chi" );

        viewPager.setAdapter(tableAdapter);
        tabLayout.setupWithViewPager( viewPager );

        return view;
    }
}
