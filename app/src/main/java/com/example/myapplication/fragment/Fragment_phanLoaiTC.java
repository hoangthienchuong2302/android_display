package com.example.myapplication.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Adapter.PhanLoaiTC_Adapter;
import com.example.myapplication.DAO.LoaiThuChiDAO;
import com.example.myapplication.R;

import com.example.myapplication.dialog.BottomSheet_PhanLoai;
import com.example.myapplication.model.LoaiThuChi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class Fragment_phanLoaiTC extends Fragment {
    FloatingActionButton fl_phanloai;
    public static PhanLoaiTC_Adapter phanLoaiTC_adapter;
    public static RecyclerView  recyclerView_PhanLoai;
    public static ArrayList<LoaiThuChi> ds_PhanLoai;
    public static LoaiThuChiDAO loaiThuChiDAO;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_phanloaitc,container,false);
        fl_phanloai= view.findViewById( R.id.fl_PhanLoai );
        recyclerView_PhanLoai=view.findViewById( R.id.rcvPhanLoaiTC );
        recyclerView_PhanLoai.setLayoutManager(new LinearLayoutManager( getContext() ));
        ds_PhanLoai= new ArrayList<>(  );
        loaiThuChiDAO = new LoaiThuChiDAO( getContext() );

        fl_phanloai.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheet_PhanLoai bottomSheet= new BottomSheet_PhanLoai();
                bottomSheet.show(getFragmentManager(),"TAG");
            }
        } );
        capNhat();
        return view;
    }

public void capNhat(){
    loaiThuChiDAO= new LoaiThuChiDAO( getContext() );
    ds_PhanLoai= loaiThuChiDAO.get();
    phanLoaiTC_adapter= new PhanLoaiTC_Adapter( ds_PhanLoai,getContext() );
    recyclerView_PhanLoai.setAdapter( phanLoaiTC_adapter );
}

}
