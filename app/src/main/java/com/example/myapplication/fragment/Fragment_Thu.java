package com.example.myapplication.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Adapter.KhoanThuAdapter;
import com.example.myapplication.Adapter.Tk_thu_Adapter;
import com.example.myapplication.DAO.KhoanThuChiDAO;
import com.example.myapplication.DAO.ThongKeDAO;
import com.example.myapplication.R;
import com.example.myapplication.model.KhoanThuChi;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Fragment_Thu extends Fragment {
    Button btn_date1, btn_date2;
    TextView tv_total;
    RecyclerView rv_tk_thu;
    public static Tk_thu_Adapter khoanThuAdapter;
    public static ArrayList<KhoanThuChi> ds_tk_thu;
    public static ArrayList<KhoanThuChi> ds_tk_thus;
    KhoanThuChiDAO khoanThuChiDAO;
    ThongKeDAO thongKeDAO;
    double total_1;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate( R.layout.fragment_tk_thu, container, false);
        btn_date1 = view.findViewById(R.id.btn_date1);
        btn_date2 = view.findViewById(R.id.btn_date2);
        tv_total = view.findViewById(R.id.tv_total);
        rv_tk_thu = view.findViewById(R.id.rv_tk_thu);
        rv_tk_thu.setLayoutManager(new LinearLayoutManager(getContext()));
        thongKeDAO= new ThongKeDAO(getContext());

        DecimalFormat formatter = new DecimalFormat("#,###");
        total_1 = thongKeDAO.getTotalthu("Thu");
        String s = formatter.format(total_1);
        tv_total.setText("Tổng tiền: "+s+" VNĐ");

        btn_date1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Date1();
            }
        });
        btn_date2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Date2();
            }
        });

        ds_tk_thu = new ArrayList<>();
        thongKeDAO= new ThongKeDAO(getContext());

        ds_tk_thu = thongKeDAO.getKhoanThu_Chi("Thu");
        khoanThuAdapter = new Tk_thu_Adapter(ds_tk_thu, getContext());
        rv_tk_thu.setAdapter(khoanThuAdapter);
        return view;
    }
    private void Date1(){
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);

        final int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        final int months = cal.get(Calendar.MONTH);
        final int years = cal.get(Calendar.YEAR);
        final Calendar calendar = Calendar.getInstance();


        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                calendar.set(i,i1,i2);
                btn_date1.setText(simpleDateFormat.format(calendar.getTime()));
            }
        },years,months,dayOfWeek);
        datePickerDialog.show();
    }

    private void Date2(){
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);

        final int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        final int months = cal.get(Calendar.MONTH);
        final int years = cal.get(Calendar.YEAR);
        final Calendar calendar = Calendar.getInstance();


        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                calendar.set(i,i1,i2);
                btn_date2.setText(simpleDateFormat.format(calendar.getTime()));

                ds_tk_thus = thongKeDAO.get_Tk_Thu(btn_date1.getText().toString(), btn_date2.getText().toString(),"Thu");
                khoanThuAdapter = new Tk_thu_Adapter(ds_tk_thus, getContext());
                rv_tk_thu.setAdapter(khoanThuAdapter);

                String date1 = btn_date1.getText().toString();
                DecimalFormat formatter = new DecimalFormat("#,###");
                double total_2 = thongKeDAO.getTotalthubyDate(date1, btn_date2.getText().toString(),"Thu");
                String s = formatter.format(total_2);
                tv_total.setText("Tổng tiền: "+s+"VNĐ");




            }
        },years,months,dayOfWeek);


        datePickerDialog.show();

    }
}
