package com.example.myapplication.dialog;

import android.app.DatePickerDialog;
import android.content.Context;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.Adapter.AdapterThu;
import com.example.myapplication.Adapter.KhoanThuAdapter;

import com.example.myapplication.DAO.KhoanThuChiDAO;

import com.example.myapplication.R;

import com.example.myapplication.model.KhoanThuChi;
import com.example.myapplication.model.LoaiThuChi;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.example.myapplication.tableFragment.Tab_KhoanThu.rv_thu;

import static com.example.myapplication.tableFragment.Tab_KhoanChi.rv_chi;

public class BottomSheet_KhoanTC extends BottomSheetDialogFragment {
    EditText ediTieuDe,edtTien,edtMoTa,edtTrangThai;
    TextView tvNgay;
    Spinner spKhoan;
    Button btnThem;
    KhoanThuChiDAO khoanThuChiDAO;
    ArrayList<KhoanThuChi> ds_khoanTC;
    ArrayList<LoaiThuChi> dsLoaiThu;
    AdapterThu adapterThu;
    KhoanThuChi khoanTC;
    public String trangthai_;
    private KhoanThuAdapter khoanThuAdapter;
    public BottomSheet_KhoanTC(){
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view   = inflater.inflate( R.layout.bottom_sheet_khoan_thu_chi,container,false );
        ediTieuDe= view.findViewById( R.id.edtTieuDe );
        edtTien  = view.findViewById( R.id.edtTien );
        edtMoTa  = view.findViewById( R.id.edtMoTa);
        tvNgay   = view.findViewById( R.id.tvNgay);
        edtTrangThai= view.findViewById( R.id.edttrangThai );
        btnThem  = view.findViewById( R.id.btnAdd );
        spKhoan  = view.findViewById( R.id.spKhoan );

        Bundle getdata = getArguments();
        trangthai_ = getdata.getString( "trangthai" );
        edtTrangThai.setText("Loai "+trangthai_);

        khoanThuChiDAO = new KhoanThuChiDAO(getContext());
        dsLoaiThu = new ArrayList<>();
        if (trangthai_.equals("Thu")){
            dsLoaiThu = khoanThuChiDAO.getAll("Thu");
        } else if (trangthai_.equals("Chi")){
            dsLoaiThu = khoanThuChiDAO.getAll("Chi");
        }

        adapterThu = new AdapterThu( getContext(), dsLoaiThu );
        spKhoan.setAdapter( adapterThu );

        Date today= new Date(  );
        final Calendar calendar=Calendar.getInstance();
        calendar.setTime( today );

        final int dayOfWeek= calendar.get(Calendar.DAY_OF_WEEK);
        final int month= calendar.get(Calendar.MONTH);
        final int year= calendar.get(Calendar.YEAR);
        tvNgay.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                DatePickerDialog datePickerDialog= new DatePickerDialog( getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        SimpleDateFormat simpleDateFormat= new SimpleDateFormat( "dd/MM/yyyy" );
                        calendar.set( i,i1,i2 );
                        tvNgay.setText( simpleDateFormat.format( calendar.getTime() ) );

                    }
                }, year,month,dayOfWeek);
                    datePickerDialog.show();
            }
        } );

        btnThem.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String date = tvNgay.getText().toString();
                String tieude = ediTieuDe.getText().toString();
                double tien = Double.parseDouble(edtTien.getText().toString());
                String mota = edtMoTa.getText().toString();
               
                int index = spKhoan.getSelectedItemPosition();
                int Maloai = dsLoaiThu.get(index).getMaLoai();

                khoanThuChiDAO = new KhoanThuChiDAO(getContext());
                khoanTC = new KhoanThuChi(tieude,date,tien,mota,Maloai);
                khoanThuChiDAO.insert(khoanTC);

                capNhat(trangthai_);
                Toast.makeText(getContext(), "Thêm thành công", Toast.LENGTH_SHORT).show();
                dismiss();
            }
        });
        return view;
    }

    public void capNhat(String trangThai){
        ds_khoanTC = khoanThuChiDAO.getKhoanThuChi( trangThai);
        khoanThuAdapter = new KhoanThuAdapter(ds_khoanTC, getContext());
        if(trangThai.equals( "Thu" )){
            rv_thu.setAdapter(khoanThuAdapter);
           khoanThuAdapter.notifyDataSetChanged();
        }else if(trangThai.equals( "Chi" )) {
            rv_chi.setAdapter( khoanThuAdapter );
            khoanThuAdapter.notifyDataSetChanged();
        }
    }
}
