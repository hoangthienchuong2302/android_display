package com.example.myapplication.dialog;

import android.app.DatePickerDialog;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.Adapter.AdapterThu;
import com.example.myapplication.Adapter.KhoanThuAdapter;
import com.example.myapplication.DAO.KhoanThuChiDAO;
import com.example.myapplication.R;
import com.example.myapplication.model.KhoanThuChi;
import com.example.myapplication.model.LoaiThuChi;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static com.example.myapplication.tableFragment.Tab_KhoanChi.rv_chi;
import static com.example.myapplication.tableFragment.Tab_KhoanThu.rv_thu;

public class BottomSheet_edit_KhoanTC extends BottomSheetDialogFragment {
    EditText ediTieuDe,edtTien,edtMoTa,edtTrangThai;
    TextView tvNgay;
    Spinner spKhoan;
    Button btnUp;
    KhoanThuChiDAO khoanThuChiDAO;
    ArrayList<KhoanThuChi> ds_khoanTC;
    ArrayList<LoaiThuChi> dsLoaiThu;
    AdapterThu adapterThu;
    KhoanThuChi khoanTC;
    public int id;
    String trangthai_;
    private KhoanThuAdapter khoanThuAdapter;
    public BottomSheet_edit_KhoanTC(){
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view   = inflater.inflate( R.layout.bottom_sheet_edit_khoan_thu_chi,container,false );
        ediTieuDe= view.findViewById( R.id.edtTieuDe );
        edtTien  = view.findViewById( R.id.edtTien );
        edtMoTa  = view.findViewById( R.id.edtMoTa);
        tvNgay   = view.findViewById( R.id.tvNgay);
        edtTrangThai= view.findViewById( R.id.edttrangThai );
        btnUp  = view.findViewById( R.id.btnAdd );
        spKhoan  = view.findViewById( R.id.spKhoan );

        Bundle args = getArguments();
        id = Integer.parseInt(args.getString("maGD"));
        String tieu_de = args.getString("tieude");
        String ngay = args.getString("ngay");
        double tien = args.getDouble("tien");
        String mota = args.getString("moTa");
        String maloai = args.getString("maloai");

        ediTieuDe.setText( tieu_de );
        final DecimalFormat decimalFormat= (DecimalFormat) NumberFormat.getInstance( Locale.US);
        decimalFormat.applyPattern( "#,###,###,###" );
        String formattedString= decimalFormat.format( tien );

        edtTien.setText( formattedString );
        edtTien.addTextChangedListener( onTextChangedListener() );
        edtMoTa.setText( mota );
        tvNgay.setText( ngay );

        trangthai_= args.getString("trangThai");
        edtTrangThai.setText(trangthai_);

        khoanThuChiDAO= new KhoanThuChiDAO( getContext() );
        dsLoaiThu = new ArrayList<>(  );
        if(trangthai_.equals( "Thu" )){
            dsLoaiThu = khoanThuChiDAO.getAll( "Thu" );
        }else if(trangthai_.equals( "Chi" )){
            dsLoaiThu = khoanThuChiDAO.getAll( "Chi" );
        }
        String ten =khoanThuChiDAO.getTen( maloai );

        adapterThu = new AdapterThu( getContext(), dsLoaiThu );
        spKhoan.setAdapter( adapterThu );

        for(int i=0; i < dsLoaiThu.size();i++){
            if(dsLoaiThu.get( i ).getTenLoai().equals( ten )){
                spKhoan.setSelection( i );
                break;
            }
        }

        Date today= new Date(  );
        final Calendar calendar=Calendar.getInstance();
        calendar.setTime( today );

        final int dayOfWeek= calendar.get(Calendar.DAY_OF_WEEK);
        final int month= calendar.get(Calendar.MONTH);
        final int year= calendar.get(Calendar.YEAR);
        tvNgay.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                DatePickerDialog datePickerDialog= new DatePickerDialog( getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        SimpleDateFormat simpleDateFormat= new SimpleDateFormat( "dd/MM/yyyy" );
                        calendar.set( i,i1,i2 );
                        tvNgay.setText( simpleDateFormat.format( calendar.getTime() ) );

                    }
                }, year,month,dayOfWeek);
                    datePickerDialog.show();
            }
        } );

        btnUp.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String date = tvNgay.getText().toString();
                String tieude = ediTieuDe.getText().toString();

                String tien = edtTien.getText().toString();
                DecimalFormat decimalFormat1=(DecimalFormat) NumberFormat.getInstance(Locale.US);
                decimalFormat1.setParseBigDecimal( true );
                BigDecimal number=null;
                try{
                    number= (BigDecimal) decimalFormat1.parse( String.valueOf( tien ) );
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                int so=Integer.parseInt( number+"" );

                String mota = edtMoTa.getText().toString();
               
                int index = spKhoan.getSelectedItemPosition();
                int Maloai = dsLoaiThu.get(index).getMaLoai();

                khoanThuChiDAO = new KhoanThuChiDAO(getContext());
                khoanTC = new KhoanThuChi(id,tieude,date,so,mota,Maloai);
                khoanThuChiDAO.update( khoanTC );

                capNhat(trangthai_);
                Toast.makeText(getContext(), "Thêm thành công", Toast.LENGTH_SHORT).show();
                dismiss();
            }
        });
        return view;
    }

    public void capNhat(String trangThai){
        ds_khoanTC = khoanThuChiDAO.getKhoanThuChi( trangThai);
        khoanThuAdapter = new KhoanThuAdapter(ds_khoanTC, getContext());

        if(trangThai.equals( "Thu" )){
            rv_thu.setAdapter(khoanThuAdapter);

        }else if(trangThai.equals( "Chi" )) {
            rv_chi.setAdapter( khoanThuAdapter );
        }
    }
    private TextWatcher onTextChangedListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                edtTien.removeTextChangedListener(this);

                try {
                    String originalString = s.toString();

                    Long longval;
                    if (originalString.contains(",")) {
                        originalString = originalString.replaceAll(",", "");
                    }
                    longval = Long.parseLong(originalString);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,###,###,###");
                    String formattedString = formatter.format(longval);

                    //setting text after format to EditText
                    edtTien.setText(formattedString);
                    edtTien.setSelection(edtTien.getText().length());
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                edtTien.addTextChangedListener(this);
            }
        };
    }
}
