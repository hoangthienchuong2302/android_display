package com.example.myapplication.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.example.myapplication.Adapter.PhanLoaiTC_Adapter;
import com.example.myapplication.DAO.LoaiThuChiDAO;
import com.example.myapplication.R;
import com.example.myapplication.fragment.Fragment_phanLoaiTC;
import com.example.myapplication.model.LoaiThuChi;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;

import static com.example.myapplication.fragment.Fragment_phanLoaiTC.ds_PhanLoai;
import static com.example.myapplication.fragment.Fragment_phanLoaiTC.phanLoaiTC_adapter;
import static com.example.myapplication.fragment.Fragment_phanLoaiTC.recyclerView_PhanLoai;

public class BottomSheet_PhanLoai extends BottomSheetDialogFragment {
    EditText ediTenLoai;
    Spinner spPhanLoai;
    Button btnThem;
    LoaiThuChiDAO loaiThuChiDAO;
    Fragment_phanLoaiTC fragment_phanLoaiTC;
    ArrayList<LoaiThuChi> ds_PhanLoais;
    public BottomSheet_PhanLoai(){
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view   = inflater.inflate( R.layout.bottom_sheet_phanloai,container,false );
        ediTenLoai= view.findViewById( R.id.edttenLoai );
        spPhanLoai= view.findViewById( R.id.spPhanLoai );
        btnThem=view.findViewById( R.id.btnAdd );
        loaiThuChiDAO=new LoaiThuChiDAO( getContext() );

        ArrayAdapter<CharSequence> adapter= ArrayAdapter.createFromResource( getContext(),
                R.array.phanLoai,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        spPhanLoai.setAdapter( adapter );

        btnThem.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tenLoai= ediTenLoai.getText().toString();
                String phanLoai=spPhanLoai.getSelectedItem().toString();
                LoaiThuChi pl = new LoaiThuChi( tenLoai,phanLoai );
                loaiThuChiDAO.insert( pl );
                Toast.makeText(getContext(),"Thêm thành công",Toast.LENGTH_SHORT).show();
                fragment_phanLoaiTC = new Fragment_phanLoaiTC();
                capNhat();
                dismiss();
            }
        } );

        return view;
    }

    public void capNhat(){
        loaiThuChiDAO= new LoaiThuChiDAO( getContext() );
        ds_PhanLoais= loaiThuChiDAO.get();
        phanLoaiTC_adapter= new PhanLoaiTC_Adapter( ds_PhanLoais,getContext() );
        recyclerView_PhanLoai.setAdapter( phanLoaiTC_adapter );
    }
}
