package com.example.myapplication.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;


public class DbHelper extends SQLiteOpenHelper
{
    static final String DbName = "QLTHUCHI";
    static final int Version = 1;
    public DbHelper(@Nullable Context context) {
        super(context, DbName, null, Version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE LOAI_TC" +
                "(maLoai INTEGER PRIMARY KEY AUTOINCREMENT  ,"+
                "tenLoai text, " +
                "trangThai text)");

        db.execSQL( "INSERT INTO LOAI_TC(tenLoai,trangThai) VALUES('Lai ngan hang','Thu')");

        db.execSQL(  "INSERT INTO LOAI_TC(tenLoai,trangThai) VALUES('Luong','Thu')");
        db.execSQL("INSERT INTO LOAI_TC(tenLoai,trangThai) VALUES('Ban hang','Thu')");
        db.execSQL("INSERT INTO LOAI_TC(tenLoai,trangThai) VALUES('Thu no','Thu')");
        db.execSQL( "INSERT INTO LOAI_TC(tenLoai,trangThai) VALUES('Sinh hoat hang ngay','Chi')");
        db.execSQL( "INSERT INTO LOAI_TC(tenLoai,trangThai) VALUES('Dong tien hoc','Chi')");
        db.execSQL("INSERT INTO LOAI_TC(tenLoai,trangThai) VALUES('Du lich','Chi')");

//        autoincrement
//        db.execSQL("CREATE TABLE KHOAN_TC(maGD INTEGER PRIMARY KEY AUTOINCREMENT, " +
//                "tieuDe text, ngay text, tien double, moTa text, maloai int, " +
//                "FOREIGN KEY (maloai) REFERENCES LOAI_TC(maloai))");
        db.execSQL("CREATE TABLE KHOAN_TC" +
                "(maGD INTEGER PRIMARY KEY AUTOINCREMENT,"+
                "tieuDe text, " +
                "ngay text," +
                "tien Double," +
                "moTa Text," +
                "maLoai integer references LOAI_TC(maLoai))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db,int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS LOAI_TC");
        db.execSQL("DROP TABLE IF EXISTS KHOAN_TC");
    }
}
