package com.example.myapplication.tableFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Adapter.KhoanThuAdapter;
import com.example.myapplication.DAO.KhoanThuChiDAO;
import com.example.myapplication.R;
import com.example.myapplication.dialog.BottomSheet_KhoanTC;
import com.example.myapplication.model.KhoanThuChi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;


public class Tab_KhoanChi extends Fragment {
    public static FloatingActionButton fl_khoanchi;
    public static RecyclerView rv_chi;
    public static KhoanThuAdapter khoanThuAdapter;
    public static ArrayList<KhoanThuChi> ds_thu;
    public static KhoanThuChiDAO khoanThuChiDAO;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_khoan_chi,container,false);
        fl_khoanchi = view.findViewById(R.id.flKhoanChi);
        rv_chi = view.findViewById(R.id.rcvKhoanChi);
        rv_chi.setLayoutManager(new LinearLayoutManager(getContext()));

        fl_khoanchi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString("trangthai", "Chi");

                BottomSheet_KhoanTC bottom_sheet = new BottomSheet_KhoanTC();
                bottom_sheet.setArguments(args);
                bottom_sheet.show(getFragmentManager(),bottom_sheet.getTag());
            }
        });

        ds_thu= new ArrayList<>();
        khoanThuChiDAO = new KhoanThuChiDAO(getContext());

        ds_thu = khoanThuChiDAO.getKhoanThuChi( "Chi" );
        khoanThuAdapter = new KhoanThuAdapter(ds_thu, getContext());
        rv_chi.setAdapter(khoanThuAdapter);

        return view;
    }
}
